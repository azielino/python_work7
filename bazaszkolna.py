import sys
# from typing import Sized


class SchoolClass:

    def __init__(self):
        self.students = []
        self.teachers = dict()
        self.sup_teacher = str()

    def add_student(self, student):
        self.students.append(student)

    def add_teacher(self, teacher, subject):
        self.teachers[teacher] = subject

    def print_sup_teacher_and_students(self):
        print(self.sup_teacher)
        for student in self.students:
            print(student)
        print("*"*70)
   
    def __repr__(self):
        return f"\n- uczniowie: {self.students}\n- nauczyciele: {self.teachers}\n- wychowawca: {self.sup_teacher}\n"


def show_classes():
 if not classes:
     print('Brak klas!')
 else:
     print(classes)

def set_class_nr():
    action = input("Podaj numer klasy (np. 3C): ")
    while True:
        if action == "":
            break
        elif not len(action) == 2 or not action[0].isnumeric() or action[1].isnumeric():
            action = input("! Podaj numer klasy (np. 3C) lub enter żeby zakończyć: ")
            continue
        else:
            break
    return action

ACTIONS = ("uczen", "nauczyciel", "wychowawca", "koniec")
action = str()
classes = dict()
sup_teacher_students = set()
teacher_sup_teachers = set()
student_teachers = list()
print("======================= START =======================")
while not action == "koniec":
    action = input(f"Wpisz polecenie z listy {ACTIONS}: ")
    if action not in ACTIONS:
        print(f"! Wpisz polecenie z listy {ACTIONS}: ")
        continue
    elif action == "uczen":
        student_name = input("Podaj imię i nazwisko ucznia: ")
        if student_name == "":
            continue
        else:
            class_nr = set_class_nr()
            if class_nr not in classes:
                classes[class_nr] = SchoolClass()
                classes[class_nr].add_student(student_name)
            else:
                classes[class_nr].add_student(student_name)
    elif action == "nauczyciel":
        teacher_name = input("Podaj imię i nazwisko nauczyciela: ")
        if teacher_name == "":
            break
        else:
            teacher_subject = input("Podaj przedmiot: ")
        while True:            
            class_nr = set_class_nr()
            if class_nr == "":
                break
            elif class_nr not in classes:
                classes[class_nr] = SchoolClass()                
                classes[class_nr].add_teacher(teacher_name, teacher_subject)
            else:
                classes[class_nr].add_teacher(teacher_name, teacher_subject)
    elif action == "wychowawca":
        sup_teacher = input("Podaj imię i nazwisko wychowawcy: ")
        if sup_teacher == "":
            break
        else:
            while True:
                class_nr = set_class_nr()
                if class_nr == "":
                    break
                elif class_nr not in classes:
                    classes[class_nr] = SchoolClass()
                    classes[class_nr].sup_teacher = sup_teacher
                else:
                    classes[class_nr].sup_teacher = sup_teacher
print(f"\nStworzone klasy:")
show_classes()
if len(sys.argv) == 2:
    phrase = sys.argv[1]
    print("*"*70)
    print(f"Fraza to: '{phrase}'")
elif len(sys.argv) == 3:
    phrase = sys.argv[1] + " " + sys.argv[2]
    print("*"*70)
    print(f"Fraza to: '{phrase}'")
elif len(sys.argv) == 4:
    phrase = sys.argv[1] + " " + sys.argv[2]+ " " + sys.argv[3]
    print("*"*70)
    print(f"Fraza to: '{phrase}'")
while True:
    if phrase in classes:
        print(f"Wychowawca i uczniowie w klasie {phrase} to:")
        classes[phrase].print_sup_teacher_and_students()
        break
    for class_nr in classes:
        if phrase == classes[class_nr].sup_teacher:
            for person in classes[class_nr].students:
                sup_teacher_students.add(person)
        elif phrase in classes[class_nr].teachers:
            teacher_sup_teachers.add(classes[class_nr].sup_teacher)
        elif phrase in classes[class_nr].students:
            print(f"Wszystkie lekcje, które ma {phrase} i nauczyciele, którzy je prowadzą to:")
            for person in classes[class_nr].teachers:
                print(classes[class_nr].teachers[person])
                print(person)
            print("*"*70)           
    if sup_teacher_students:
        print(f"Wychowawca {phrase} prowadzi uczniów:")
        for person in sup_teacher_students:
            print(person)
        print("*"*70)
        break
    elif teacher_sup_teachers:
        print(f"Wychowawcy wszystkich klas, z którymi ma zajęcia {phrase} to:")
        for person in teacher_sup_teachers:
            print(person)
        print("*"*70)
        break
    break
print("======================= STOP =======================")