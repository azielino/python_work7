# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ Funkcje, obiekty, PEP @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

# owner = {"Imię": "Adam", "Nazwisko": "Kowalski"}
# rows = [
# ("Styczeń", {"Saldo": 1200, "Wpłaty": 1800, "Wydatki": 1300}), 
# ("Luty", {"Saldo": 1300, "Wpłaty": 1900, "Wydatki": 1200}), 
# ]
# 
# print("*" * 10)
# print("Właściciel konta")
# print("*" * 10)
# for k,v in owner.items():
#     print("{}: {}".format(k, v))
# print("*" * 10)
# 
# 
# for name, row in rows:  
#     print("*" * 10)
#     print("Saldo miesięczne: {}".format(name))
#     print("*" * 10)
#     for k,v in row.items():
#   	    print("{}: {}".format(k, v))
#     print("*" * 10)

# def name_genitive(name):
# 	if name[-1] == "a":
# 		return "{}y".format(name[:-1])
# 	if name[-1] != "a":
# 		return "{}a".format(name)
# 
# names = ["Adam", "Ewa", "Łukasz", "Iwona"]
# print(names[0][-1]) # ------------------------------------ wypisuje ostatni znak stringu "Adam" czyli "m"
# print(names[0][:-1]) # ------------------------------------ wypisuje string "Adam" bez ostatniego znaku czyli "Ada"
# print(names[0][:-3]) # ------------------------------------ wypisuje string "Adam" bez 3 ostatnich znaków czyli "A"
# for firstname in names:
# 	print("Wysyłam zaproszenie do {}".format(name_genitive(firstname)))

# def divider(mul=10, char="*"):
# 	print(char * mul)
# 
# divider()
# divider(15)
# divider(20, "-")
# divider(char="-")
# a = 10
# print(id(a))
# a = a+ 10
# print(id(a))
# 
# sample_dict= {"a": 10}
# print(id(sample_dict))
# sample_dict["a"] += 10
# print(id(sample_dict))

# class User: # tworzymy klasę o nazwie "User"
# 	def __init__ (self, firstname, lastname): # przy tworzeniu obiektu będą wymagane dwa parametry: firstname, lastname
# 		self.firstname = firstname # parametr firstname zapiszemy wewnątrz obiektu jako właściwość firstname
# 		self.lastname = lastname # parametr lastname zapiszemy wewnątrz obiektu jako właściwość lastname		
# 
# 	def print_hello(self): #ta metoda nie używa parametrów, tylko obiektu, w którym jest zadeklarowana
# 		print("Hello {} {}".format(self.firstname, self.lastname))
# 
# 	def print_bye(self): #ta metoda nie używa parametrów, tylko obiektu, w którym jest zadeklarowana
# 		print("Bye {} {}".format(self.firstname, self.lastname))
# 
# user1 = User("Ewa", "Nowak")
# user2 = User("Jan", "Kowalski")
# 
# user1.print_hello()
# user2.print_hello()
# user1.print_bye()
# user2.print_bye()
# User.print_hello(user1)
# user1.lastname = "Nowacka"
# user1.print_hello()

# sup_teacher_students = set()
# sup_teacher_students.add("jan nowak")
# sup_teacher_students.add("adam")
# sup_teacher_students.add("jan nowak")
# if sup_teacher_students:
#     for person in sup_teacher_students:
#         print(person)

# action = input("Podaj numer klasy (np. 3C): ")# -------------------------------------------pętla sprawdzania czy
# sign = ""
# while not sign:
#     if len(action) == 2:
#         for i in action:
#             if i.isnumeric():
#                 sign = i
#                 print(f"{sign} to liczba")
#                 break
#             else:
#                 action = input("! Podaj numer klasy (np. 3C): ")
#                 break
#     else:
#         action = input("! Podaj numer klasy (np. 3C): ")
#         continue
# print(f"Numer klasy to {action[-1]}")


# def set_class_nr():
#    action = input("Podaj numer klasy (np. 3C): ")
#    while not len(action) == 2 or not action[0].isnumeric() or action[1].isnumeric():
#        action = input("! Podaj numer klasy (np. 3C): ")
#    return action
# 
# clas_nr = set_class_nr()
# print(clas_nr)

while True:
    print("wpisz cos albo enter zeby zakonczyc: ")
    cos = input()
